import random
from datetime import datetime

from faker import Faker


faker = Faker()


def generate_message() -> str:
    host = '10.0.0.5'
    dt = datetime.now().strftime('%Y/%m/%d %H:%M:%S')
    method = faker.http_method()
    endpoint = faker.uri_path()
    status = random.choice([200, 201, 300, 301, 302, 303, 400, 401, 403, 404, 500, 502])
    bytes_ = random.randrange(11111, 22222)
    user_agent = faker.chrome()
    host2 = faker.ipv4()

    return f'{host} - - [{dt}] "{method} {endpoint} HTTP/1.1" {status} {bytes_} "-" "{user_agent}" "{host2}"'


def infinity():
    while True:
        yield
