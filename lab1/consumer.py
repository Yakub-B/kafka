#!/usr/bin/env python

import asyncio
import logging

from aiokafka import AIOKafkaConsumer

import settings


logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger('kafka-consumer')


async def consume():
    logger.info(f'Starting consumer on {settings.KAFKA_SERVER}')
    consumer = AIOKafkaConsumer(settings.TOPIC, bootstrap_servers=settings.KAFKA_SERVER)
    await consumer.start()

    try:
        async for msg in consumer:
            logger.info(
                f'Consumed message from {msg.topic}: {msg.value.decode("utf-8")}'
            )
    finally:
        logger.info('Stopping consumer')
        await consumer.stop()


async def main():
    await consume()


if __name__ == '__main__':
    asyncio.run(main())
