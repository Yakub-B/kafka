#!/usr/bin/env python

import asyncio
import logging
import argparse

from aiokafka import AIOKafkaProducer

from utils import generate_message, infinity
import settings


logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger('kafka-producer')


async def send_one_msg():
    producer = AIOKafkaProducer(bootstrap_servers=settings.KAFKA_SERVER)
    await producer.start()
    try:
        await producer.send_and_wait(settings.TOPIC, b'Hello Kafka')
    finally:
        await producer.stop()


async def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--count', '-c', help='How many messages generate. Use -1 to send infinite messages', default=10, type=int
    )
    parser.add_argument('--timeout', '-t', help='Timeout (seconds) between sending messages', default=0.5, type=float)
    args = parser.parse_args()

    producer = AIOKafkaProducer(bootstrap_servers=settings.KAFKA_SERVER)
    await producer.start()
    logger.info('Start producing messages')

    rng = infinity() if args.count == -1 else range(args.count)
    try:
        for _ in rng:
            msg = generate_message()
            await producer.send_and_wait(settings.TOPIC, msg.encode('utf-8'))
            await asyncio.sleep(args.timeout)
    finally:
        logger.info('Stopping producer')
        await producer.stop()


if __name__ == '__main__':
    asyncio.run(main())
